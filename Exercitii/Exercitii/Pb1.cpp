#include<iostream>
#include<vector>
#include<algorithm>

void citire(std::vector<int> &vec) {
	int dim, val;
	std::cout << "Nr de elemente din vector este: ";
	std::cin >> dim;
	for (int i = 0; i < dim; i++) {
		std::cin >> val;
		vec.push_back(val);
	}
}

void afisare(std::vector<int> vec) {
	std::cout << "Elementele vectorului sunt: ";
	for (auto& i : vec) {
		std::cout << i << " ";
	}
	std::cout << std::endl;
}

void suma_doua_numere(int val1, int val2) {
	auto suma = [](auto v1, auto v2) {
		return v1 + v2;
	};
	std::cout << suma(val1, val2) << "\n";
}

void sortare(std::vector<int> &vec) {
	sort(vec.begin(), vec.end(),
		[](const int& val1, const int& val2) {return val1 > val2; });
}

int suma(std::vector<int> vec) {
	int sum = 0;
	for_each(vec.begin(), vec.end(),
		[&](int i) { sum += i; });
	return sum;
}

int main()
{
	int alegere;
	std::vector<int> vec;
	citire(vec);

	std::cout << "0. EXIT!\n";
	std::cout << "1. Afisarea vectorului.\n";
	std::cout << "2. Sortarea vectorului.\n";
	std::cout << "3. Cautarea unui element in vector.\n";
	std::cout << "4. Adunarea elementelor din vector.\n";
	std::cout << "5. Adunarea a doua numere.\n";

	while (1) {
		std::cout << "Introduceti una dintre optiunile de mai sus: ";
		std::cin >> alegere;
		switch (alegere) {
		case 0:
			return 0;
		case 1:
			afisare(vec);
			break;
		case 2:
			sortare(vec);
			break;
		case 3:
			sort(vec.begin(), vec.end());
			int val;
			std::cout << "Valoarea cautata este: ";
			std::cin >> val;
			if (binary_search(vec.begin(), vec.end(), val))
				std::cout << "Valoarea a fost gasita.\n";
			else
				std::cout << "Valoarea nu a fost gasita.\n";
			break;
		case 4:
			std::cout << "Suma elementelor din vector este: " << suma(vec) << ".\n";
			break;
		case 5: {
			int nr1, nr2;
			std::cout << "nr1= ";
			std::cin >> nr1;
			std::cout << "nr2= ";
			std::cin >> nr2;
			suma_doua_numere(nr1, nr2);
			break;
		}
		}
	}

	system("pause");
	return 0;
}